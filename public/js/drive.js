function createChart(canvas, labels, name, points) {
  const context = canvas.getContext("2d");
  return new Chart(context, {
    type: "line",
    data: {
      labels,
      datasets: [
        {
          borderColor: "#9b4dca",
          pointRadius: 1,
          backgroundColor: "#ebd1ff",
          pointBackgroundColor: "#9b4dca",
          pointBorderColor: "#9b4dca",
          pointHoverBackgroundColor: "#9b4dca",
          pointHoverBorderColor: "#9b4dca",
          label: name,
          data: points,
          borderWidth: 1,
        },
      ],
    },
    options: {
      scales: {
        xAxes: [
          {
            type: "time",
          },
        ],
      },
    },
  });
}

function loadDrive(refs) {
  const pathArray = window.location.pathname.split("/");
  const key = pathArray[2];

  fetch(`/api/drives/${key}?limit=3000`)
    .then((res) => res.json())
    .then((res) => {
      const { name, description, data } = res;
      refs.driveName.innerHTML = name;
      refs.driveDescription.innerHTML = description;

      const { labels, points } = parseData(data);

      for (const group in points) {
        const row = document.createElement("div");
        row.className = "row";
        const column = document.createElement("div");
        column.className = "column";
        const canvas = document.createElement("canvas");

        createChart(canvas, labels, group, points[group]);

        column.appendChild(canvas);
        row.appendChild(column);
        refs.main.appendChild(row);
      }
    })
    .catch((err) => {
      console.error(err);
      refs.alert.innerHTML = "Failed to load data";
    });
}

function parseData(data) {
  return data.reduce(
    (acc, point) => {
      const { labels, points } = acc;
      const { fields, timestamp } = point;

      labels.unshift(moment(timestamp * 1000).format("YYYY/MM/DD HH:mm:ss"));

      for (const field in fields) {
        const fieldData = points[field] || [];
        fieldData.unshift({ t: new Date(timestamp * 1000), y: fields[field] });
        points[field] = fieldData;
      }

      return acc;
    },
    { labels: [], points: {} }
  );
}
