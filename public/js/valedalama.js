function submitDrive(form, refs) {
  event.preventDefault();

  const body = {
    name: form.elements["nameInput"].value,
    description: form.elements["descriptionInput"].value,
  };

  const options = {
    method: "POST",
    body: JSON.stringify(body),
    headers: {
      "Content-Type": "application/json",
    },
  };

  fetch("/api/drives", options)
    .then((res) => res.json())
    .then((res) => {
      refs.createFormInfo.innerHTML =
        "Created the drive! Be sure to make note of the keys below.";
      refs.publicKey.innerHTML = res.publicKey;
      refs.accessKey.innerHTML = res.accessKey;
      refs.keyTable.classList.remove("hidden");
    })
    .catch((err) => {
      console.error(err);
      refs.createFormAlert.innerHTML =
        "Something went wrong creating the drive!";
    });
}

function viewDrive(form) {
  event.preventDefault();

  const key = form.elements["keyInput"].value;
  location.href = `/drives/${key}`;
}
