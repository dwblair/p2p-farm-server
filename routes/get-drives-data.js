const db = require("../db");
const PouchDB = require("../pouch");

async function routes(fastify, options) {
  fastify.route({
    method: "GET",
    url: "/api/drives/:publicKey",
    schema: {
      response: {
        name: { type: "string" },
        description: { type: "string" },
        data: {
          type: "array",
          items: {
            type: "object",
            required: ["_id", "deviceId", "fields", "publicKey", "timestamp"],
            properties: {
              _id: { type: "string" },
              _rev: { type: "string" },
              deviceId: { type: "string" },
              deviceName: { type: "string" },
              fields: { type: "object" },
              publicKey: { type: "string" },
              timestamp: { type: "integer" },
            },
          },
        },
      },
    },
    handler: async (request, reply) => {
      const { publicKey } = request.params;
      const limit = request.query["limit"] || 1000;

      db.get(
        "SELECT * FROM drives WHERE publicKey = ?",
        [publicKey],
        (err, drive) => {
          if (!drive) {
            reply.code(404);
            reply.send(
              new Error("Couldn't find that drive, is that the right key?")
            );
          }

          const pouch = new PouchDB(`./data/pouchdb/${drive.name}`);
          pouch
            .allDocs({
              include_docs: true,
              descending: true,
              limit: limit,
            })
            .then((result) => {
              const data = result.rows.map((row) => row.doc);
              reply.send({
                name: drive.name,
                description: drive.description,
                data,
              });
            })
            .catch((err) => {
              console.log(err);
              reply.code(500);
              reply.send(new Error("Failed to get drive data"));
            });
        }
      );

      await reply;
    },
  });
}

module.exports = routes;
