const db = require("../db");
const sync = require("../sync");

async function routes(fastify, options) {
  fastify.route({
    method: "POST",
    url: "/api/drives/:publicKey/sync",
    preHandler: fastify.auth([fastify.verifyAccess]),
    handler: async (request, reply) => {
      const { publicKey } = request.params;

      db.get(
        "SELECT * FROM drives WHERE publicKey = ?",
        [publicKey],
        (err, drive) => {
          if (!drive) {
            reply.code(404);
            reply.send(
              new Error("Couldn't find that drive, is that the right key?")
            );
          }

          sync(drive).then((res) => {
            if (res.ok) {
              reply.code(204);
              reply.send();
            } else {
              reply.code(500);
              reply.send(new Error("Failed to write the data to hyperdrive."));
            }
          });
        }
      );

      await reply;
    },
  });
}

module.exports = routes;
