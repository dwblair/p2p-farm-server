const db = require("../db");
const PouchDB = require("../pouch");

async function routes(fastify, options) {
  fastify.route({
    method: "PUT",
    url: "/api/drives/:publicKey",
    schema: {
      body: {
        type: "object",
        required: ["deviceId", "fields"],
        properties: {
          deviceId: { type: "string" },
          deviceName: { type: "string" },
          fields: { type: "object" },
        },
      },
    },
    preHandler: fastify.auth([fastify.verifyAccess]),
    handler: async (request, reply) => {
      const { publicKey } = request.params;

      db.get(
        "SELECT * FROM drives WHERE publicKey = ?",
        [publicKey],
        (err, drive) => {
          if (!drive) {
            reply.code(404);
            reply.send(
              new Error("Couldn't find that drive, is that the right key?")
            );
          }

          const timestamp = new Date().getTime();
          const data = {
            ...request.body,
            _id: timestamp.toString(),
            publicKey,
            timestamp: Math.round(timestamp / 1000),
          };

          const pouch = new PouchDB(`./data/pouchdb/${drive.name}`);
          pouch
            .put(data)
            .then(() => {
              reply.code(204);
              reply.send();
            })
            .catch((err) => {
              console.log(err);
              reply.code(500);
              reply.send(new Error("Failed to put drive data"));
            });
        }
      );

      await reply;
    },
  });
}

module.exports = routes;
