const postDrives = require("./post-drives");
const getDrivesData = require("./get-drives-data.js");
const putDrivesData = require("./put-drives-data.js");
const postDrivesSync = require("./post-drives-sync.js");

async function routes(fastify, options) {
  fastify.get("/drives/:publicKey", (request, reply) => {
    return reply.sendFile("drive.html");
  });

  postDrives(fastify, options);
  getDrivesData(fastify, options);
  putDrivesData(fastify, options);
  postDrivesSync(fastify, options);
}

module.exports = routes;
