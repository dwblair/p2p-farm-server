const crypto = require("crypto");
const db = require("../db");
const fs = require("fs");
const hyperdrive = require("hyperdrive");
const net = require("net");
const PouchDB = require("../pouch");
const replicate = require("@hyperswarm/replicator");

const INSERT_DRIVE =
  "INSERT INTO drives (name, description, publicKey) VALUES (?, ?, ?)";
const INSERT_KEY =
  "INSERT INTO keys (accessKey, publicKey, valid) VALUES (?, ?, ?)";

async function routes(fastify, options) {
  fastify.route({
    method: "POST",
    url: "/api/drives",
    schema: {
      body: {
        type: "object",
        required: ["name"],
        properties: {
          name: { type: "string" },
          description: { type: "string" },
        },
      },
      response: {
        publicKey: { type: "string" },
        accessKey: { type: "string" },
      },
    },
    handler: async (request, reply) => {
      const { body } = request;
      const path = `./data/hyperdrive/${body.name}`;

      if (fs.existsSync(path)) {
        const err = new Error();
        err.statusCode = 409;
        err.message =
          "There's already a stream with that name, try another one!";
        throw err;
      }

      const drive = hyperdrive(path);
      const pouch = new PouchDB(`./data/pouchdb/${body.name}`);

      drive.on("ready", () => {
        const publicKey = drive.key.toString("hex");
        const salt = crypto.randomBytes(256);
        const accessKey = crypto
          .createHash("sha256")
          .update(publicKey)
          .update(salt)
          .digest("hex");

        db.run(INSERT_KEY, [accessKey, publicKey, true]);
        db.run(INSERT_DRIVE, [body.name, body.description, publicKey]);

        drive.writeFile(
          "/index.json",
          JSON.stringify({ title: body.name, description: body.description })
        );
        const swarm = replicate(drive);

        reply.send({ publicKey, accessKey });
      });

      await reply;
    },
  });
}

module.exports = routes;
