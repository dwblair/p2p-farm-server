const hyperdrive = require("hyperdrive");
const PouchDB = require("./pouch");

function sync({ name }) {
  const drive = hyperdrive(`./data/hyperdrive/${name}`);
  const stream = drive.createWriteStream("/pouch.txt");
  const pouch = new PouchDB(`./data/pouchdb/${name}`);
  return pouch.dump(stream);
}

module.exports = sync;
