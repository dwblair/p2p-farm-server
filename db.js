const fs = require("fs");
const sqlite3 = require("sqlite3").verbose();

if (!fs.existsSync("./data")) {
  fs.mkdirSync("./data");
}

const db = new sqlite3.Database("./data/db.sqlite");

module.exports = db;
