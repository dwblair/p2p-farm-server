const CronJob = require("cron").CronJob;
const db = require("./db");
const fs = require("fs");
const fastify = require("fastify")({ logger: true });
const hyperdrive = require("hyperdrive");
const path = require("path");
const replicate = require("@hyperswarm/replicator");
const routes = require("./routes");
const sync = require("./sync");

fastify
  .decorate("verifyAccess", (request, reply, done) => {
    params = [request.raw.headers.authorization, request.params.publicKey];
    db.get(
      "SELECT valid FROM keys WHERE accessKey = ? AND publicKey = ?",
      params,
      (err, result) => {
        if (result) {
          done();
        } else {
          done(
            new Error("That's not a valid access key for the specified drive!")
          );
        }
      }
    );
  })
  .register(require("fastify-auth"))
  .register(require("fastify-static"), {
    root: path.join(__dirname, "public"),
  })
  .after(() => routes(fastify));

const setup = async () => {
  await fs.mkdirSync("./data/pouchdb", { recursive: true });

  await new Promise((resolve, reject) => {
    db.run(
      `
      CREATE TABLE drives (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        name TEXT,
	      description TEXT,
        publicKey TEXT
      )
    `,
      () => resolve()
    );
  });

  await new Promise((resolve, reject) => {
    db.run(
      `
      CREATE TABLE keys (
        accessKey TEXT PRIMARY KEY,
        publicKey TEXT,
	      valid BOOLEAN
      )
    `,
      () => resolve()
    );
  });

  await new Promise((resolve, reject) => {
    db.each(
      "SELECT * FROM drives",
      [],
      (err, row) => {
        const { name } = row;
        const drive = hyperdrive(`./data/hyperdrive/${name}`);
        const swarm = replicate(drive);
        console.log(`Replicating drive ${name}`);
      },
      () => resolve()
    );
  });
};

const syncJob = new CronJob("0 0 * * * *", () => {
  db.each("SELECT * FROM drives", (err, drive) => {
    sync(drive).then((res) => {
      if (!res.ok) {
        console.log(
          `Failed to automatically sync ${drive.name} drive at ${new Date()}`
        );
      }
    });
  });
});

const start = async () => {
  try {
    syncJob.start();
    await setup();
    await fastify.listen(3000);
  } catch (err) {
    fastify.log.error(err);
    process.exit(1);
  }
};

start();
